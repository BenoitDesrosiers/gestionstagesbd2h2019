﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/*
 * titre           :EntreprisesCtrl.cs
 * description     :Point de départ du démo de l'application à réaliser pour
 *                  le TP2 et TP3 du cours 420-BD2-DM
 *                  L'application permet de gérer les stages en permettant de créer
 *                  les entreprises, départements et contacts. 
 * author          :Benoit Desrosiers
 * date de création:20190314
 * usage           :
 * notes           :
 */
namespace TP2_BD2_H2019
{
    class EntreprisesCtrl
    {
        public void MenuPrincipal(tp3_2Entities context)
        {
            List<String> optionsMenu = new List<String>();
            optionsMenu.Add("1) Ajout d'une Entreprise");
            optionsMenu.Add("2) Effacer une Entreprise");
            optionsMenu.Add("3) Modifier une Entreprise");
            optionsMenu.Add("4) Lister les Entreprises");
            optionsMenu.Add("0) sortir");

            View menu = new View();
            int choix;
            do
            {
                menu.AfficheMenu(optionsMenu);
                choix = menu.ChoisirOption(new List<int> { 0, 1, 2, 3, 4});

                if (choix != 0)
                {
                    switch (choix)
                    {
                        case 1:
                            this.Ajouter(context);
                            break;
                        case 2:
                            this.Effacer(context);
                            break;
                        case 3:
                            this.Modifier(context);
                            break;
                        case 4:
                            this.Lister(context);
                            break;

                    }

                    Console.WriteLine("");

                }
            } while (choix != 0);
        }

        /// <summary>
        /// Ajout d'une nouvelle Entreprise
        /// </summary>
        /// <param name="context">la connection à la bd</param>
        private  void Ajouter(tp3_2Entities context)
        {
            View view = new View();
            Entreprise nouvelleEntreprise = new Entreprise();

            nouvelleEntreprise.nom = view.InputString("Nom de l'Entreprise: ");
            nouvelleEntreprise.description = view.InputString("Description de l'Entreprise: ");
            nouvelleEntreprise.telephone = view.InputString("Téléphone de l'Entreprise: ");

            // confirmation et ajout dans la BD
            Console.WriteLine("Voulez-vous vraiment ajouter :");
            Console.WriteLine("{0} {1} {2} ",
                nouvelleEntreprise.nom,
                nouvelleEntreprise.description,
                nouvelleEntreprise.telephone
                );
            char ajouter = view.InputChar("O/N", new List<char> { 'O', 'N' }, true);
            if (ajouter == 'O')
            {
                context.Entreprises.Add(nouvelleEntreprise);
                context.SaveChanges();
            }


        }

        /// <summary>
        /// Efface une Entreprise
        /// </summary>
        /// <param name="context"> la connection à la BD.</param>
        private void Effacer(tp3_2Entities context)
        {
            this.Lister(context);
            View view = new View();
            int choix = view.InputInt("Id de l'Entreprise à effacer (0 pour annuler) : ");
            if (choix != 0)
            {
                Entreprise Entreprise = context.Entreprises.Find(choix);
                if (Entreprise != null)
                {
                    this.Affiche(Entreprise);
                    Char effacer = view.InputChar("Effacer cette Entreprise O/N ", new List<char> { 'O', 'N' }, true);
                    if (effacer == 'O')
                    {
                        //La contrainte on delete cascade des contacts a été enlevée dans la bd pour éviter une boucle
                        // on doit donc effacer manuellement les contacts. 
                        List<Contact> lesContactsdeLentreprise = Entreprise.Contacts.ToList();
                        foreach(Contact unContact in lesContactsdeLentreprise)
                        {
                            context.Contacts.Remove(unContact);
                        }
                        context.Entreprises.Remove(Entreprise);
                        context.SaveChanges();
                    }
                } else
                {
                    Console.WriteLine("Mauvais id");
                }    
            }
        }

        /// <summary>
        /// Modifier une Entreprise existante
        /// </summary>
        /// <param name="context">la connection à la bd</param>
        private void Modifier(tp3_2Entities context)
        {
            this.Lister(context);
            View view = new View();
            int choix = view.InputInt("Id de l'entreprise à modifier (0 pour annuler) : ");
            if (choix != 0)
            {
                Entreprise Entreprise = context.Entreprises.Find(choix);
                if (Entreprise != null)
                {

                    Entreprise.nom = view.InputString("Nom de l'Entreprise: ", Entreprise.nom.Trim());
                    Entreprise.description = view.InputString("Description de l'Entreprise: ", Entreprise.description.Trim());
                    Entreprise.telephone = view.InputString("Téléphone de l'Entreprise: ", Entreprise.telephone.Trim());

                    // confirmation et ajout dans la BD
                    Console.WriteLine("Voulez-vous vraiment sauvegarder ces valeurs :");
                    Console.WriteLine("{0} {1} {2} ",
                        Entreprise.nom,
                        Entreprise.description,
                        Entreprise.telephone
                        );
                    char sauvegarder = view.InputChar("O/N", new List<char> { 'O', 'N' }, true);
                    if (sauvegarder == 'O')
                    {
                        context.SaveChanges();
                    }
                } else
                {
                    Console.WriteLine("Mauvais id");
                }
            }

        }

        /// <summary>
        /// Affiche l'id et le nom des Entreprises
        /// </summary>
        /// <param name="context"></param>
        public void Lister(tp3_2Entities context)
        {
            foreach (Entreprise Entreprise in context.Entreprises)
            {
                Console.WriteLine("id: {0}  nom: {1}", Entreprise.id, Entreprise.nom.Trim());
            }
        }

        private void Affiche(Entreprise Entreprise)
        {
            Console.WriteLine("id: {0}", Entreprise.id);
            Console.WriteLine("nom: {0}", Entreprise.nom.Trim());
            Console.WriteLine("téléphone: {0}", Entreprise.telephone.Trim());
            Console.WriteLine("description: {0}", Entreprise.description.Trim());
        }
    }
}
