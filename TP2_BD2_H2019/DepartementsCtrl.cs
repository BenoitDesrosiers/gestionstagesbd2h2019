﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/*
 * titre           :DepartementsCtrl.cs
 * description     :Gestion de la classe Departement
 *                  Permet de faire le CRUD d'un département
 * author          :Benoit Desrosiers
 * date de création:20190314
 * usage           :
 * notes           :
 */

namespace TP2_BD2_H2019
{
    class DepartementsCtrl
    {
        /// <summary>
        /// Le menu principal pour la gestion des départements
        /// </summary>
        /// <param name="context">La connexion à la BD</param>
        public void MenuPrincipal(tp3_2Entities context)
        {
            List<String> optionsMenu = new List<String>();
            optionsMenu.Add("1) Ajout d'un département");
            optionsMenu.Add("2) Effacer un département");
            optionsMenu.Add("3) Modifier un département");
            optionsMenu.Add("4) Lister tous les départements");

            optionsMenu.Add("0) sortir");

            View menu = new View();
            int choix;
            do
            {
                menu.AfficheMenu(optionsMenu);
                choix = menu.ChoisirOption(new List<int> { 0, 1, 2, 3, 4, 5});

                if (choix != 0)
                {
                    switch (choix)
                    {
                        case 1:
                            this.Ajouter(context);
                            break;
                        case 2:
                            this.Effacer(context);
                            break;
                        case 3:
                            this.Modifier(context);
                            break;
                        case 4:
                            this.Lister(context);
                            break;

                    }

                    Console.WriteLine("");

                }
            } while (choix != 0);
        }

        /// <summary>
        /// Ajout d'un nouveau départements
        /// </summary>
        /// <param name="context">la connection à la bd</param>
        private  void Ajouter(tp3_2Entities context)
        {
            View view = new View();
            Departement nouveauDepartement = new Departement();

            nouveauDepartement.nom = view.InputString("Nom du département: ");

            EntreprisesCtrl entreprises = new EntreprisesCtrl();
            entreprises.Lister(context);
            int choix = view.InputInt("Id de l'entreprise à associer : ");
            Entreprise entrepriseAssocie = context.Entreprises.Find(choix);
            if (entrepriseAssocie == null)
            {
                Console.WriteLine("mauvais id");
            } else {
                // confirmation et ajout dans la BD
                Console.WriteLine("Voulez-vous vraiment sauvegarder ces valeurs :");
                Console.WriteLine("{0} associé à {1} ",
                    nouveauDepartement.nom,
                    entrepriseAssocie.nom
                    );
                char sauvegarder = view.InputChar("O/N", new List<char> { 'O', 'N' }, true);
                if (sauvegarder == 'O')
                {
                    nouveauDepartement.Entreprise = entrepriseAssocie;
                    context.Departements.Add(nouveauDepartement);
                    context.SaveChanges();
                }
            }
        }

        /// <summary>
        /// Efface un département
        /// </summary>
        /// <param name="context"> la connexion à la BD.</param>
        private void Effacer(tp3_2Entities context)
        {
            this.Lister(context);
            View view = new View();
            int choix = view.InputInt("Id du département à effacer (0 pour annuler) : ");
            if (choix != 0)
            {
                Departement departementAEffacer = context.Departements.Find(choix);
                if (departementAEffacer != null)
                {
                    this.Affiche(departementAEffacer);
                    Char effacer = view.InputChar("Effacer ce département O/N ", new List<char> { 'O', 'N' }, true);
                    if (effacer == 'O')
                    {
                        context.Departements.Remove(departementAEffacer);
                        context.SaveChanges();
                    }
                } else
                {
                    Console.WriteLine("Mauvais id");
                }    
            }
        }

        /// <summary>
        /// Modifier un Departement existant
        /// </summary>
        /// <param name="context">la connexion à la bd</param>
        private void Modifier(tp3_2Entities context)
        {
            this.Lister(context);
            View view = new View();
            int choix = view.InputInt("Id du département à modifier (0 pour annuler) : ");
            if (choix != 0)
            {
                Departement departementAModifier = context.Departements.Find(choix);
                if (departementAModifier != null)
                {
                    EntreprisesCtrl entreprises = new EntreprisesCtrl();
                    entreprises.Lister(context);
                    int idEntreprise = view.InputInt("Id de l'entreprise à associer : ", departementAModifier.entrepriseId);
                    Entreprise entrepriseAssocie = context.Entreprises.Find(idEntreprise);
                    if (entrepriseAssocie == null)
                    {
                        Console.WriteLine("mauvais id");
                    }
                    else
                    {
                        departementAModifier.nom = view.InputString("Nom du département: ", departementAModifier.nom.Trim());
                        // confirmation et ajout dans la BD
                        Console.WriteLine("Voulez-vous vraiment sauvegarder ces valeurs :");
                        Console.WriteLine("{0} associé à {1} ",
                            departementAModifier.nom,
                            entrepriseAssocie.nom
                            );
                        char sauvegarder = view.InputChar("O/N", new List<char> { 'O', 'N' }, true);
                        if (sauvegarder == 'O')
                        {
                            departementAModifier.Entreprise = entrepriseAssocie;
                            context.SaveChanges();
                        }
                    }
                } else
                {
                    Console.WriteLine("Mauvais id");
                }
            }

        }

        /// <summary>
        /// Affiche l'id et le nom de tous les Departements
        /// </summary>
        /// <param name="context">La connexion à la bd</param>
        public void Lister(tp3_2Entities context)
        {
            foreach (Departement departement in context.Departements)
            {
                Console.WriteLine("id: {0}  nom: {1} associé à {2}", departement.id, departement.nom.Trim(), departement.Entreprise.nom);
            } 
        }

        /// <summary>
        /// Affichage de base d'un département
        /// </summary>
        /// <param name="unDepartement">Le département à afficher</param>
        private void Affiche(Departement unDepartement)
        {
            Console.WriteLine("id: {0}", unDepartement.id);
            Console.WriteLine("nom: {0}", unDepartement.nom);
            Console.WriteLine("de l'entreprise {0}", unDepartement.Entreprise.nom.Trim());
        }
    }
}
