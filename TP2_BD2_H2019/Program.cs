﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


/*
 * titre           :Program.cs
 * description     :Point de départ du démo de l'application à réaliser pour
 *                  le TP2 et TP3 du cours 420-BD2-DM
 *                  L'application permet de gérer les stages en permettant de créer
 *                  les entreprises, départements et contacts. 
 * author          :Benoit Desrosiers
 * date de création:20190314
 * usage           :
 * notes           :
 */

namespace TP2_BD2_H2019
{
    
    class Program
    {
        /// <summary>
        /// Menu principal de l'application.
        /// </summary>
        static void Main(string[] args)
        {
            List<String> optionsMenu = new List<string>();

            optionsMenu.Add("1) Gestion des Entreprises");
            optionsMenu.Add("2) Gestion des Départements");
            optionsMenu.Add("3) Gestion des Contacts");

            optionsMenu.Add("0) sortir");

            View menu = new View();
            int choix;

            do
            {
                menu.AfficheMenu(optionsMenu);
                choix = menu.ChoisirOption(new List<int> { 0, 1, 2, 3, 4, 5 });
                if (choix != 0)
                {

                    tp3_2Entities context = new tp3_2Entities();
                    using (context)
                    {
                        switch (choix)
                        {
                            case 1:
                                EntreprisesCtrl menuEntreprise = new EntreprisesCtrl();
                                menuEntreprise.MenuPrincipal(context);
                                break;
                            case 2:
                                DepartementsCtrl menuDepartement = new DepartementsCtrl();
                                menuDepartement.MenuPrincipal(context);
                                break;
                            case 3:
                                ContactsCtrl menuContact = new ContactsCtrl();
                                menuContact.MenuPrincipal(context);
                                break;
                            default:
                                break;
                        }
                    }
                }
            } while (choix != 0);
        }


    }
    
}
