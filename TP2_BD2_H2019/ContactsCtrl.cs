﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/*
 * titre           :ContactsCtrl.cs
 * description     :Gestion de la classe Contact
 *                  Permet de faire le CRUD d'un Contact
 * author          :Benoit Desrosiers
 * date de création:20190314
 * usage           :
 * notes           :
 */

namespace TP2_BD2_H2019
{
    class ContactsCtrl:UtilisateursCtrl
    {
        /// <summary>
        /// Le menu principal pour la gestion des contacts
        /// </summary>
        /// <param name="context"></param>
        public void MenuPrincipal(tp3_2Entities context)
        {
            List<String> optionsMenu = new List<String>();
            optionsMenu.Add("1) Ajout d'un contact d'entreprise");
            optionsMenu.Add("2) Effacer un contact");
            optionsMenu.Add("3) Modifier un contact");
            optionsMenu.Add("4) Lister tous les contacts");
            optionsMenu.Add("5) Ajout d'un contact de département");

            optionsMenu.Add("0) sortir");

            View menu = new View();
            int choix;
            do
            {
                menu.AfficheMenu(optionsMenu);
                choix = menu.ChoisirOption(new List<int> { 0, 1, 2, 3, 4, 5 });

                if (choix != 0)
                {
                    switch (choix)
                    {
                        case 1:
                            this.AjouterAEntreprise(context);
                            break;
                        case 2:
                            this.Effacer(context);
                            break;
                        case 3:
                            this.Modifier(context);
                            break;
                        case 4:
                            this.Lister(context);
                            break;
                        case 5:
                            this.AjouterADepartement(context);
                            break;
                    }
                    Console.WriteLine("");
                }
            } while (choix != 0);
        }

        /// <summary>
        /// Ajout d'un nouveau contact d'entreprise
        /// </summary>
        /// <param name="context">la connexion à la BD</param>
        private void AjouterAEntreprise(tp3_2Entities context)
        {
            View view = new View();
            Contact nouveauContact = new Contact();

            EntreprisesCtrl entreprises = new EntreprisesCtrl();
            entreprises.Lister(context);
            int choix = view.InputInt("Id de l'entreprise à associer : ");
            Entreprise entrepriseAssocie = context.Entreprises.Find(choix);
            if (entrepriseAssocie == null)
            {
                Console.WriteLine("mauvais id");
            }
            else
            {
                InputInfoContact(view, nouveauContact);
                nouveauContact.Entreprise = entrepriseAssocie;
                Console.WriteLine("Voulez-vous vraiment sauvegarder ces valeurs :");
                this.Affiche(nouveauContact);
                char sauvegarder = view.InputChar("O/N", new List<char> { 'O', 'N' }, true);
                if (sauvegarder == 'O')
                {
                    context.Contacts.Add(nouveauContact);
                    context.SaveChanges();
                }
            }
        }

        /// <summary>
        /// Demande les infos d'un contact
        /// </summary>
        /// <param name="view">La View à utiliser pour l'affichage</param>
        /// <param name="unContact">Le contact à modifier. Est changé par cette fonction</param>
        private  void InputInfoContact(View view, Contact unContact)
        {
            unContact.Utilisateur = new Utilisateur();
            this.InputInfoUtilisateur(view, unContact.Utilisateur);
            unContact.role = view.InputString("Role :");
        }

        /// <summary>
        /// Efface un contact de la BD
        /// </summary>
        /// <param name="context">la connexion à la bd</param>
        private void Effacer(tp3_2Entities context)
        {
            this.Lister(context);
            View view = new View();
            int choix = view.InputInt("Id du contact à effacer (0 pour annuler) :");
            if(choix != 0)
            {
                Contact contactAEffacer = context.Contacts.Find(choix);
                if (contactAEffacer != null)
                {
                    this.Affiche(contactAEffacer);
                    Char effacer = view.InputChar("Effacer ce département O/N ", new List<char> { 'O', 'N' }, true);
                    if (effacer == 'O')
                    {
                        context.Utilisateurs.Remove(contactAEffacer.Utilisateur);
                        context.Contacts.Remove(contactAEffacer);
                        context.SaveChanges();
                    }

                } else
                {
                    Console.WriteLine("Mauvais id");
                }
            }

        }

        /// <summary>
        /// Permet de modifier un contact 
        /// </summary>
        /// <param name="context">la connexion à la bd</param>
        private void Modifier(tp3_2Entities context)
        { }

        /// <summary>
        /// Affiche tous les contacts
        /// </summary>
        /// <param name="context">la connexion à la bd</param>
        private void Lister(tp3_2Entities context)
        {
            foreach(Contact unContact in context.Contacts)
            {
                Console.WriteLine("id : {0} {1} {2}", unContact.id, 
                    unContact.Utilisateur.prenom.Trim(), 
                    unContact.Utilisateur.nom.Trim());
            }
        }

        /// <summary>
        /// Création d'un nouveau contact et association à un département
        /// </summary>
        /// <param name="context">la connexion à la bd</param>
        private void AjouterADepartement(tp3_2Entities context)
        {
            View view = new View();
            Contact nouveauContact = new Contact();

            DepartementsCtrl departement = new DepartementsCtrl();
            departement.Lister(context);
            int choix = view.InputInt("Id du département à associer : ");
            Departement departementAssocie = context.Departements.Find(choix);
            if (departementAssocie == null)
            {
                Console.WriteLine("mauvais id");
            }
            else
            {
                InputInfoContact(view, nouveauContact);
                nouveauContact.Departements.Add(departementAssocie);  

                Console.WriteLine("Voulez-vous vraiment sauvegarder ces valeurs :");
                this.Affiche(nouveauContact);
                char sauvegarder = view.InputChar("O/N", new List<char> { 'O', 'N' }, true);
                if (sauvegarder == 'O')
                {
                    context.Contacts.Add(nouveauContact);
                    context.SaveChanges();
                }
            }
        }

        /// <summary>
        /// Affiche les infos d'un contacts
        /// </summary>
        /// <param name="unContact">le contact à afficher</param>
        private void Affiche(Contact unContact)
        {
            Console.WriteLine("prenom : {0}", unContact.Utilisateur.prenom);
            Console.WriteLine("nom : {0}", unContact.Utilisateur.nom);
            Console.WriteLine("login : {0}", unContact.Utilisateur.login);
            // le mot de passe ne peut être affiché
            Console.WriteLine("téléphone : {0}", unContact.Utilisateur.telephone);
            Console.WriteLine("courriel : {0}", unContact.Utilisateur.courriel);
            Console.WriteLine("role : {0}", unContact.role);


        }
    }

   


}
