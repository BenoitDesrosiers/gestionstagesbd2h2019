﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/*
 * titre           :ViewCtrl.cs
 * description     :Gestion de l'affichage.
 *                  Fourni des routines pour l'entrée de données.
 *                  Cette classe est une première étape vers la séparation
 *                  des controlleurs et de l'affichage/entrées de données.
 * author          :Benoit Desrosiers
 * date de création:20190314
 * usage           :
 * notes           :
 */
namespace TP2_BD2_H2019
{
    class View
    {

        /// <summary>
        /// Affiche un menu
        /// </summary>
        /// <param name="lignesMenu">Une liste de string à afficher</param>
        public void AfficheMenu(List<string> lignesMenu)
        {
            foreach (string ligne in lignesMenu)
            {
                Console.WriteLine(ligne);
            }
        }

        /// <summary>
        /// Permet de choisir une option dans une liste d'option
        /// </summary>
        /// <param name="choixValides">La liste des entiers pouvant être utilisés</param>
        /// <param name="question">La question. Une valeur par défaut est fournie</param>
        /// <returns>un entier qui est dans la liste des choix valides</returns>
        public int ChoisirOption(List<int> choixValides, string question = "Votre choix: ")
        {
            int choix;

            do
            {
                Console.Write(question);
                string c_choix = Console.ReadLine();
                if (!Int32.TryParse(c_choix, out choix))
                { choix = choixValides.Min() - 1; } //valide que la valeur est dans les choix valides.
            } while (!choixValides.Contains(choix));

            return choix;

        }

        /// <summary>
        /// Permet de faire l'entrée d'une chaine de caractère
        /// </summary>
        /// <param name="question">La question à poser</param>
        /// <param name="defaut">La valeur a utiliser si l'usager n'entre rien (return)</param>
        /// <returns>La chaine de caractère entrée, ou la valeur par defaut</returns>
        public string InputString(string question, string defaut = null)
        {
            Console.Write(question);
            if (defaut != null)
            {
                Console.Write(" ( " + defaut + " )");
            }
            string reponse = Console.ReadLine();
            if (reponse == "" && defaut != null)
            {
                reponse = defaut;
            }
            return reponse;
        }

        /// <summary>
        /// Permet de faire l'entrée d'un seul caractère
        /// </summary>
        /// <param name="question">La question à poser</param>
        /// <param name="choixValides">Les caractères pouvant être utilisés</param>
        /// <param name="majuscule">si vrai, le caractère entré sera retourné en majuscule. Faux par défaut</param>
        /// <returns>un caractère qui est dans la liste des choix valides</returns>
        public char InputChar(string question, List<char> choixValides, bool majuscule = false)
        {
            string c_choix;
            do
            {
                Console.Write(question);
                c_choix = Console.ReadLine();
                if (majuscule)
                    c_choix = c_choix.ToUpper();
            } while (!choixValides.Contains(c_choix[0]));

            return c_choix[0];
        }

        /// <summary>
        /// Permet de faire l'entrée d'une valeur entière
        /// </summary>
        /// <param name="question">La question à poser</param>
        /// <param name="defaut">La valeur a utiliser si l'usager n'entre rien (return)</param>
        /// <returns>un entier</returns>
        public int InputInt(string question, int? defaut = null)
        {
            string reponse;
            int reponseInt;
            do
            {
                Console.Write(question);
                if (defaut != null)
                {
                    Console.Write(" ( " + defaut + " )");
                }
                reponse = Console.ReadLine();
                if (reponse == "" && defaut != null)
                {
                    reponse = defaut.ToString();
                }
            } while (!Int32.TryParse(reponse, out reponseInt));
            return reponseInt;

        }

        /// <summary>
        /// Permet de faire l'entrée d'une date
        /// </summary>
        /// <param name="question">La question à poser</param>
        /// <returns>Une date</returns>
        public DateTime InputDate(string question)
        {
            string reponse;
            DateTime reponseDate = DateTime.Now;
            bool bonneDate;

            do
            {

                Console.Write(question);
                reponse = Console.ReadLine();
                bonneDate = true;
                try
                {
                    reponseDate = Convert.ToDateTime(reponse);
                }
                catch (Exception)
                {
                    bonneDate = false;
                }
            } while (!bonneDate);
            return reponseDate;
        }
    }
}
